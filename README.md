# Encoder PCB

This is a breakout board for the [AEAT-8800-Q24](https://www.digikey.com/product-detail/en/broadcom-limited/AEAT-8800-Q24/516-3787-ND/6681014?utm_adgroup=Sensors%20&%20Transducers) magnetic encoder.

### Prereqs:
- KiCAD 5.1.4+
- Digikey KiCAD libraries (https://github.com/digikey/digikey-kicad-library)

